#include "GameEngine.h"

int main(int argc, char* argv[]){
    GameEngine *game = new GameEngine("My GameEngine");
    game->run();
    delete game;
    return EXIT_SUCCESS;
}
