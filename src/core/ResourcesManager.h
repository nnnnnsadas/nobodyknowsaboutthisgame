#ifndef RESOURCESMANAGER_H_
#define RESOURCESMANAGER_H_

#include <vector>

#ifdef __GNUC__
#include <tr1/unordered_map>
#elif(_MSC_VER == 1700)
#include <unordered_map>
#endif

#include <string>
#include <iostream>

#include "../components/Resource.h"

    /* based on this:
    http://immersedcode.org/2011/4/8/stacked-resource-manager/
    */

class ResourcesManager {
private:
    std::vector<std::tr1::unordered_map<std::string, Resource*>> resources;
public:
    ResourcesManager();
    virtual ~ResourcesManager();
    void push();
    void pop();
    template <class T> T *get(const std::string &filename) // used once only for getting resource. i.e. Texture *weapon = resmgr.get<Texture>("weapon.png");
    {
        std::tr1::unordered_map<std::string, Resource*>::iterator iter;
        for (int i = resources.size() - 1; i >= 0; i--) {
            iter = resources[i].find(filename);
            if (iter != resources[i].end()) {
                T *ptr = dynamic_cast<T *>(iter->second);
                if (!(ptr))
                {
                    std::cerr << "resource pointer issue: " << "'" << iter->first << "'" << std::endl;
                    exit(EXIT_FAILURE);
                }
                return ptr;
            }
        }
        T *r = new T(this);
        r->load(filename);
        resources[resources.size() - 1][filename] = r;
        return r;
    };
};

#endif /* RESOURCESMANAGER_H_ */
