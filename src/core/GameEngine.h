#ifndef APPSTATESMANAGER_H_
#define APPSTATESMANAGER_H_
#define _CRT_SECURE_NO_WARNINGS

#include "SDL.h"
#include <string>

class AppState;
class IntroState;
class MenuState;
class PlayState;
class Settings;

class GameEngine {
private:
    AppState *currentState;
    IntroState *introState;
    MenuState *menuState;
    PlayState *playState;
    Uint32 curr_ticks, prev_ticks;

    void render();
public:
    Settings * settings;
    GameEngine(std::string = "SDL2/OpenGL");
    virtual ~GameEngine();
    enum states {Intro, Menu, Play};
    void changeState(states);
    void run();
    void shutdown();
};

#endif /* APPSTATESMANAGER_H_ */
