#ifndef SETTINGS_H_
#define SETTINGS_H_

#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "SDL_Net.h"
#include "string.h"
#include <fstream>
#include <iostream>
using namespace std;

class Settings
{
private:
    unsigned int pxw;
    unsigned int pxh;
    int scw;
    int sch;
    bool fullscreen;
    void save();
    void load();
    std::string gamename;
    void initOpenGL();
    SDL_Window *window;
    SDL_DisplayMode displayMode;
public:
    SDL_GLContext glcontext;
    Settings(std::string gamename_ = "SDL2/OpenGL");
    ~Settings();
    unsigned int getPixW();
    unsigned int getPixH();
    unsigned int getScrW();
    unsigned int getScrH();
    bool getFs();
    void apply(unsigned int w, unsigned int h, bool Fs);
    SDL_Window * getWindow();
};

#endif /* SETTINGS_H_ */
