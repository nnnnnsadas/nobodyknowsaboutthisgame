#include "Settings.h"

Settings::Settings(std::string gamename_) {
    gamename = gamename_;
    load();
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        std::cerr << "unable to init SDL: " << SDL_GetError() << std::endl;
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GetDesktopDisplayMode(0, &displayMode);
    scw = displayMode.w;
    sch = displayMode.h;
    if (fullscreen)
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, scw, sch, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
    else
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, pxw,pxh, SDL_WINDOW_OPENGL);
    glcontext = SDL_GL_CreateContext(window);
    initOpenGL();
    int flags = IMG_INIT_JPG | IMG_INIT_PNG;
    int initted = IMG_Init(flags);
    if ((initted & flags) != flags)
    {
        std::cerr << "IMG_Init: " << IMG_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }
    TTF_Init();
    atexit(SDL_Quit);
    atexit(IMG_Quit);
    atexit(TTF_Quit);
}

Settings::~Settings() {
    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
}

void Settings::initOpenGL() {
    glClearColor (0, 0, 0, 1);

    glShadeModel( GL_SMOOTH );

    if(fullscreen)
        glViewport( 0, 0, scw, sch);
    else
        glViewport( 0, 0, pxw, pxh );

    glEnable( GL_TEXTURE_2D );

    // Enable transparency
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

    glOrtho( 0, pxw, pxh, 0, -1, 1 );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );

    //  Check for error
    GLenum error = glGetError();
    if( error != GL_NO_ERROR )
        std::cerr << "error initializing OpenGL: " << error << std::endl;
}


unsigned int Settings::getPixW() {
    return pxw;
}

unsigned int Settings::getPixH() {
    return pxh;
}

bool Settings::getFs() {
    return fullscreen;
}

void Settings::apply(unsigned int _w, unsigned int _h, bool Fs) {
    pxw = _w;
    pxh = _h;
    fullscreen = Fs;
    SDL_GL_DeleteContext(glcontext);
    SDL_DestroyWindow(window);
    if (fullscreen)
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, scw, sch, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
    else
        window = SDL_CreateWindow(
                gamename.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, pxw, pxh, SDL_WINDOW_OPENGL);
    glcontext = SDL_GL_CreateContext(window);
    save();
    initOpenGL();
}

void Settings::save() {
    ofstream out("rsc//saves//settings.dat");
    out << pxw << endl;
    out << pxh << endl;
    out << fullscreen << endl;
    out.close();
}

void Settings::load() {
    if (ifstream("rsc//saves//settings.dat") == NULL)
    {
        pxw = 640;
        pxh = 480;
        fullscreen = false;
    }
    else
    {
        ifstream in("rsc//saves//settings.dat");
        in >> pxw;
        in >> pxh;
        in >> fullscreen;
        in.close();
    }
}

SDL_Window * Settings::getWindow() {
    return window;
}