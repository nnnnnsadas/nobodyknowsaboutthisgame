#include "GameEngine.h"
#include "../appstates/AppState.h"
#include "../appstates/IntroState.h"
#include "../appstates/PlayState.h"
#include "../appstates/MenuState.h"
#include "Settings.h"

GameEngine::GameEngine(std::string gamename) {
    freopen("std.log", "w", stdout);
    freopen("err.log", "w", stderr);
    settings = new Settings(gamename);
    std::vector<std::string> v;
    v.push_back("rsc//1.jpg");
    v.push_back("rsc//1.jpg");
    v.push_back("rsc//1.jpg");
    introState = new IntroState(v);
    menuState = new MenuState();
    playState = new PlayState();
    currentState = introState;
}

GameEngine::~GameEngine() {
    delete settings;
    delete introState;
    delete menuState;
    delete playState;
}

void GameEngine::changeState(states state) {
    switch (state)
    {
    case Intro:
        currentState = introState;
        return;
    case Menu:
    	currentState = menuState;
    	return;
    case Play:
        currentState = playState;
        return;
    }
}

void GameEngine::render()
{
    glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
        
	glLoadIdentity();
        
	currentState->render(this);
        
	SDL_GL_SwapWindow(this->settings->getWindow());
}

void GameEngine::run() 
{
    // this article was very helpful
    // http://gameprogrammingpatterns.com/game-loop.html
    double MS_PER_UPDATE = 1000.0/60;
	prev_ticks = SDL_GetTicks();
	Uint32 lag = 0.0;
    currentState->running = true;
	while (currentState->running)
	{
		curr_ticks = SDL_GetTicks();
		double elapsed = curr_ticks - prev_ticks;
		prev_ticks = curr_ticks;
		lag += elapsed;
		currentState->processInput(this);
		while (lag >= MS_PER_UPDATE)
		{
		    currentState->update(this);
		    lag -= MS_PER_UPDATE;
		}
		render();
	}
}

void GameEngine::shutdown() {
	currentState->running = false;
}