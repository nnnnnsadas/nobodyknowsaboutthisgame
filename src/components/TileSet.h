#ifndef TILESET_H_
#define TILESET_H_

#include <iostream>

#include "Resource.h"

class Tile;

class ResourcesManager;

class TileSet: Resource {
    friend ResourcesManager;
private:
    unsigned int amount;
    Tile **tiles;
    void load(const std::string) override;
public:
    TileSet(ResourcesManager* const resmgr) : Resource(resmgr) { tiles = nullptr; amount = 0; };
    virtual ~TileSet();
    Tile *getTile(unsigned int);
};

#endif /* TILESET_H_ */
