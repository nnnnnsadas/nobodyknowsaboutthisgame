#include "Texture.h"

Texture::~Texture() {
    glDeleteTextures(1, &textureid);
}

void Texture::load(const std::string  filename) {
    SDL_Surface *surface;
    int mode;
    if (!(surface = IMG_Load(filename.c_str())) )
    {
        std::cerr << "couldnt load image file '" << filename.c_str() << "'" << std::endl;
    }
    // work out what format to tell glTexImage2D to use...

    if (surface->format->BytesPerPixel == 3) // RGB 24bit
    {
        if(surface->format->Rmask==0x000000ff)
            mode = GL_RGB;
        else
            mode = GL_BGR;
    }
    else if (surface->format->BytesPerPixel == 4)  // RGBA 32bit
    {
        if(surface->format->Rmask==0x000000ff)
            mode = GL_RGBA;
        else
            mode = GL_BGRA;
    }

    // create one texture name
    glGenTextures(1, &textureid);

    // tell opengl to use the generated texture name
    glBindTexture(GL_TEXTURE_2D, textureid);

    origw=surface->w;
    origh=surface->h;

    // this reads from the sdl surface and puts it into an opengl texture
    glTexImage2D(GL_TEXTURE_2D, 0, mode, surface->w, surface->h, 0, mode, GL_UNSIGNED_BYTE, surface->pixels);

    // these affect how this texture is drawn later on...
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);

    // clean up
    SDL_FreeSurface(surface);
}

void Texture::render(double x, double y, unsigned int offs_x, unsigned int offs_y, unsigned int w_, unsigned int h_) {
    unsigned int w, h;
    w = w_;
    h = h_;
    if (w_ == 0)
        w = origw;
    if (h_ == 0)
        h = origh;

    if (w_ > origw || h_ > origh)
        std::cerr << "crop more than original size. x=" << x << " y=" << y << std::endl;

    // tell opengl to use the generated texture name
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScaled(1/double(origw), 1/double(origh), 1);

    glBindTexture(GL_TEXTURE_2D, textureid);

    glEnable(GL_TEXTURE_2D);
    // make a rectangle

    glBegin(GL_QUADS);

    // top left
    glTexCoord2i(offs_x, offs_y);
    glVertex3i(x, y, 0);

    // top right
    glTexCoord2i(offs_x+w, offs_y+0);
    glVertex3i(x+w, y, 0);

    // bottom right
    glTexCoord2i(offs_x+w, offs_y+h);
    glVertex3i(x+w, y+h, 0);

    // bottom left
    glTexCoord2i(offs_x, offs_y+h);
    glVertex3i(x, y+h, 0);

    glEnd();

    glDisable(GL_TEXTURE_2D );
}
