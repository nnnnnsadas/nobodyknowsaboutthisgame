#include "Text.h"

Text::Text()
{
    setFont("rsc//font.TTF");
    setSize(20);
    setColor(255, 255, 255);
    setText("Text");
    upd();
}

Text::~Text()
{
    glDeleteTextures(1, &textureid);
}

void Text::setFont(std::string filename)
{
    this->fontfilename = filename;
}

void Text::render(double X, double Y)
{
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, textureid);
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.f,1.f,1.f);
    glBegin(GL_QUADS);
    glTexCoord2d(0, 0); glVertex2d(X, Y);
    glTexCoord2d(1, 0); glVertex2d(X+w, Y);
    glTexCoord2d(1, 1); glVertex2d(X+w, Y+h);
    glTexCoord2d(0, 1); glVertex2d(X, Y+h);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

void Text::setText(std::string text_)
{
    text = text_;
}

void Text::setSize(unsigned int size)
{
    fontsize = size;
}

void Text::setColor(unsigned char R, unsigned char G, unsigned char B)
{
    color.r = R;
    color.g = G;
    color.b = B;
}

void Text::upd()
{
    TTF_Font *font;
    if (!(font = TTF_OpenFont(fontfilename.c_str(),fontsize)) )
        std::cerr << "couldnt load font file '" << fontfilename.c_str() << "'" << std::endl;
    SDL_Surface *message;
    if (!(message = TTF_RenderText_Blended(font, text.c_str(), color)) )
        std::cerr << "couldnt load '" << text.c_str() << "'"<< std::endl;
    TTF_CloseFont(font);
    w=message->w;
    h=message->h;

    /*Generate an OpenGL 2D texture from the SDL_Surface*.*/
    glGenTextures(1, &textureid);
    glBindTexture(GL_TEXTURE_2D, textureid);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA,
        GL_UNSIGNED_BYTE, message->pixels);

    SDL_FreeSurface(message);
}

std::string Text::getText() {
	return text;
}

unsigned int Text::getW() {
    return w;
}

unsigned int Text::getH() {
    return h;
}