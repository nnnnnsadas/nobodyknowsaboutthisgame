#ifndef TILE_H_
#define TILE_H_

class Texture;

class Tile {
protected:
    unsigned int t_offs_x, t_offs_y;
    Texture *texture;
public:
    Tile();
    Tile(Texture*, unsigned int = 0, unsigned int = 0);
    virtual ~Tile();
    virtual void render(double, double);
    const static unsigned char size = 16;
};

#endif /* TILE_H_ */
