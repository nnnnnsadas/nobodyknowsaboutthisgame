#include "MenuItem.h"
#include "../core/GameEngine.h"
#include "../appstates/MenuState.h"

MenuItem::MenuItem(unsigned int X_, unsigned int Y_, std::string text_, void(*act)(GameEngine*, MenuState*)) {
    x = X_;
    y = Y_;
    text.setText(text_);
    this->setNotCurrent();
    activate_ = act;
}

MenuItem::~MenuItem() {
}

void MenuItem::setCurrent() {
    text.setSize(40);
    text.setColor(255,0,0);
    text.upd();
}

void MenuItem::setNotCurrent()
{
    text.setSize(30);
    text.setColor(255,255,255);
    text.upd();
}

void MenuItem::setPos(int x_, int y_) {
    x = x_;
    y = y_;
}

void MenuItem::render() {
    text.render(x, y);
}

void MenuItem::activate(GameEngine *engine, MenuState *ms) {
    activate_(engine, ms);
}

void MenuItem::setText(std::string text_) {
    text.setText(text_);
    text.upd();
}

bool MenuItem::isMouseInside(int x_, int y_) {
    if (x_>=x && x_< x+text.getW() && y_>=y && y_<=y+text.getH())
        return true;
    else
        return false;
}

MenuItem *MenuItem::copy() {
    return new MenuItem(x, y, text.getText(), activate_);
}
