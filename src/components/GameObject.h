#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

class GameObject {
protected:
    double x, y;
public:
    GameObject() { x = y = 0; };
    virtual ~GameObject(){};
    virtual void render(double, double) = 0;
    virtual void setPos(double, double) = 0;
    virtual void getPos(double&, double&) = 0;
    virtual bool checkCollision(GameObject*) = 0;
};

#endif /* GAMEOBJECT_H_ */
