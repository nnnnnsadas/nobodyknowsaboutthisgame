#ifndef BLOCK_H_
#define BLOCK_H_

#include "GameObject.h"

class Tile;

class Block : public GameObject {
private:
    Tile *tile;
public:
    Block(Tile*, double, double);
    virtual ~Block();
    virtual void render(double, double) override;
    virtual void setPos(double, double) override;
    virtual void getPos(double&, double&) override;
    virtual bool checkCollision(GameObject*) override;
};

#endif /* BLOCK_H_ */
