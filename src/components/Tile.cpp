#include "Tile.h"
#include "Texture.h"

Tile::Tile() {
    texture = nullptr;
    t_offs_x = t_offs_y = 0;
}

Tile::Tile(Texture *t_,unsigned int t_offs_x_, unsigned int t_offs_y_) {
    texture = t_;
    t_offs_x = t_offs_x_;
    t_offs_y = t_offs_y_;
}

Tile::~Tile() {
}

void Tile::render(double x, double y) {
    texture->render(x, y, t_offs_x, t_offs_y, size, size);
}
