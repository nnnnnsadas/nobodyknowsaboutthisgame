#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_image.h"
#include <iostream>

#include "Resource.h"

class ResourcesManager;

class Texture: public Resource
{
    friend ResourcesManager;
private:
    GLuint textureid;
    unsigned int origw, origh;
    Texture(ResourcesManager* const resmgr) : Resource(resmgr) { origw = origh = textureid = 0; };
    ~Texture(void);
    void load(const std::string) override;
public:
    void render(double, double, unsigned int offs_x = 0, unsigned int offs_y = 0, unsigned int w = 0, unsigned int h = 0);
};

#endif /* TEXTURE_H_ */
