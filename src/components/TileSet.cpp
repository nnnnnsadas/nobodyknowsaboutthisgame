#include "TileSet.h"
#include "Tile.h"
#include "Texture.h"
#include "../core/ResourcesManager.h"
#include "../dependence/rapidxml/rapidxml.hpp"
#include "../dependence/rapidxml/rapidxml_utils.hpp"
#include "../dependence/rapidxml/rapidxml_print.hpp"

using namespace rapidxml;

TileSet::~TileSet() {
    for (unsigned int i=0; i < amount; i++)
    {
        delete tiles[i];
    }
    delete [] tiles;
}

void TileSet::load(std::string filename) {

    std::ifstream inFile(filename);

    if(!inFile)
    {
        std::cerr << "could not load tileset: " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    //Dump contents of file into a string
    std::string xmlContents;

    //Blocked out of preference
    {
        std::string line;
        while(std::getline(inFile, line))
            xmlContents += line;
    }

    //Convert string to rapidxml readable char*
    std::vector<char> xmlData = std::vector<char>(xmlContents.begin(), xmlContents.end());
    xmlData.push_back('\0');

    //Create a parsed document with &xmlData[0] which is the char*
    xml_document<> doc;
    doc.parse<parse_no_data_nodes>(&xmlData[0]);

    //Get the root node
    xml_node<>* root = doc.first_node();

    amount = atoi(root->first_attribute("tiles")->value());

    tiles = new Tile*[amount];
    for (unsigned int i=0; i<amount; i++)
        tiles[i] = nullptr;

    //Load each necessary tileset
    Texture *texture = resMgr->get<Texture>((root->first_attribute("file")->value()));

    xml_node<>* tile = root->first_node("tile");
    while(tile)
    {
        //Get all the attributes
        if(tiles[atoi(tile->first_attribute("id")->value())] == nullptr)
            tiles[atoi(tile->first_attribute("id")->value())] =
                    new Tile(texture, atoi(tile->first_attribute("offset_x")->value()), atoi(tile->first_attribute("offset_y")->value()));
        tile = tile->next_sibling("tile");
    }
}

Tile* TileSet::getTile(unsigned int id) {
    return tiles[id];
}
