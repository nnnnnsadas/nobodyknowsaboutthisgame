#ifndef LEVEL_H_
#define LEVEL_H_

#include <string>
#include <fstream>
#include <ctime>

class ResourcesManager;
class GameObject;

class Level {
private:
    ResourcesManager *resMgr;
    void loadTileSet(const std::string&);
    GameObject ***objects;
    unsigned int w, h;
public:
    Level(ResourcesManager*);
    virtual ~Level();
    void render(double, double);
    void update(float);
};

#endif /* LEVEL_H_ */
