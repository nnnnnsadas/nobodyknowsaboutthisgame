#ifndef TEXT_H_
#define TEXT_H_

#include "SDL_ttf.h"
#include "SDL_opengl.h"
#include <string>
#include <iostream>

class Text
{
private:
    std::string text;
    std::string fontfilename;
    GLuint textureid;
    unsigned int fontsize;
    unsigned int w, h;
    SDL_Color color;
public:
    void setFont(std::string filename);
    void setText(std::string text);
    std::string getText();
    void setSize(unsigned int size);
    void setColor(unsigned char, unsigned char, unsigned char);
    void render(double X, double Y);
    void upd();
    unsigned int getW(), getH();
    Text();
    ~Text();
};
#endif /* TEXT_H_ */
