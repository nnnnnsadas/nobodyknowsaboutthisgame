#include "Level.h"
#include "Tile.h"
#include "TileSet.h"
#include "../core/ResourcesManager.h"
#include "../components/GameObject.h"
#include "../components/Block.h"
#include "../components/Tile.h"
#include "../components/Texture.h"

Level::Level(ResourcesManager *resMgr_) {
    resMgr = resMgr_;
    w = 64;
    h = 32;
    objects = new GameObject**[w];
    for (unsigned int i=0; i<w; i++)
    {
        objects[i] = new GameObject*[h];
        for (unsigned int j=0; j<h; j++)
            objects[i][j] = nullptr;
    }
    srand(time(0));

    for (unsigned int i=0; i<w; i++)
    {
        int temp;
        if ( i==0 )
            temp = h/2;
        else
        {
            int temp_ = 0;
            while (objects[i-1][temp_] == nullptr)
                temp_++;
            temp = temp_;
        }
        int a = rand() % 2;
        rand() % 2 == 1 ? temp += a : temp -= a;
        for (unsigned int j=temp; j<h; j++)
        {
            objects[i][j] = new Block(new Tile(resMgr->get<Texture>("rsc//tiles//tileset.png"), 0, 0), i * Tile::size, j * Tile::size);
        }
    }
}

Level::~Level() {
    for (unsigned int i=0; i<w; i++)
    {
        for (unsigned int j=0; j<h; j++)
        {
			if(objects[i][j]!=nullptr)
				delete objects[i][j];
        }
        delete [] objects[i];
    }
    delete [] objects;
}

void Level::render(double camerax, double cameray) {
    for (unsigned int i=-camerax/Tile::size; i<w; i++)
    {
        for (unsigned int j=-cameray/Tile::size; j<h; j++)
        {
            if (objects[i][j] != nullptr)
                objects[i][j]->render(camerax, cameray);
        }
    }
}

void Level::update(float ticks) {
}
