#ifndef RESOURCE_H_
#define RESOURCE_H_

#include <string>

class ResourcesManager;

class Resource {
    friend ResourcesManager;
protected:
    ResourcesManager *resMgr;
    virtual void load(const std::string filename) = 0;
    Resource(ResourcesManager * const resmgr_) { resMgr = resmgr_; };
    virtual ~Resource() {} ;
};

#endif /* RESOURCE_H_ */
