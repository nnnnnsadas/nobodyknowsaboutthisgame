#pragma once
class AbstractWidget
{
protected:
    int x, y;
public:
    AbstractWidget(void);
    virtual ~AbstractWidget(void);
    virtual bool isMouseInside(int, int) = 0;
    virtual void render() = 0;
};