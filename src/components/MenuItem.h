#ifndef MENUITEM_H_
#define MENUITEM_H_

#include <string>

#include "Text.h"
#include "AbstractWidget.h"

class GameEngine;
class MenuState;

class MenuItem: public AbstractWidget {
private:
    void (*activate_)(GameEngine*, MenuState*);
    Text text;
public:
    MenuItem(unsigned int, unsigned int, std::string, void(*)(GameEngine*, MenuState*));
    virtual ~MenuItem();
    void setCurrent();
    void setNotCurrent();
    void render();
    void activate(GameEngine*, MenuState*);
    void setText(std::string);
    void setPos(int, int);
    bool isMouseInside(int, int);
    MenuItem* copy();
};

#endif /* MENUITEM_H_ */
