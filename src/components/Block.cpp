#include "Block.h"
#include "Tile.h"

Block::Block(Tile *tile_, double x_, double y_) {
    tile = tile_;
    x = x_;
    y = y_;
}

Block::~Block() {
}

void Block::render(double camerax, double cameray)
{
    tile->render((x + camerax), (y + cameray));
}

void Block::setPos(double x_, double y_) {
    x = x_;
    y = y_;
}

void Block::getPos(double &x_, double &y_) {
    x_ = x;
    y_ = y;
}

bool Block::checkCollision(GameObject *obj) {
    return false;
}
