#ifndef TESTSTATE_H_
#define TESTSTATE_H_

#include <SDL.h>
#include <SDL_opengl.h>
#include <GL/gl.h>
#include <queue>

#include "AppState.h"
#include "../core/ResourcesManager.h"

class Level;

class PlayState: public AppState 
{
private:
    std::queue<std::string> levelsqueue;
    Level *lvl;
    double camerax, cameray;
public:
    PlayState();
    virtual ~PlayState();
    void processInput(GameEngine *engine);
    void render(GameEngine *engine);
    void update(GameEngine *engine);
};

#endif /* TESTSTATE_H_ */
