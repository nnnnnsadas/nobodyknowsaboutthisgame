#ifndef INTROSTATE_H_
#define INTROSTATE_H_

#include <SDL.h>
#include <SDL_opengl.h>
#include <GL/gl.h>
#include <queue>

#include "AppState.h"
#include "../core/ResourcesManager.h"
#include "../components/Texture.h"

class IntroState: public AppState {
private:
    std::queue<Texture*> queue;
    double curTime;
    static const unsigned int maxTime = 5000;
    static const unsigned int fadeTime = 1500;
public:
    IntroState(std::vector<std::string>);
    virtual ~IntroState();
    void render(GameEngine *engine);
    void processInput(GameEngine *engine);
    void update(GameEngine *engine);
};

#endif /* INTROSTATE_H_ */
