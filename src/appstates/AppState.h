#ifndef APPSTATE_H_
#define APPSTATE_H_

#include "SDL.h"

class GameEngine;
class ResourcesManager;

class AppState
{
protected:
    ResourcesManager *resMgr;
    const Uint8 * keystate;
    void standardEventsHandling(GameEngine*, SDL_Event);
public:
    AppState();
    static bool running;
    virtual void render(GameEngine *engine) = 0;
    virtual void processInput(GameEngine *engine) = 0;
    virtual void update(GameEngine *engine) = 0;
    virtual ~AppState();
};

#endif /* APPSTATE_H_ */
