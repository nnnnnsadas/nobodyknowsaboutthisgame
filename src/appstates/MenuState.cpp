#include "MenuState.h"
#include "../core/GameEngine.h"
#include "../core/Settings.h"
#include "../components/MenuItem.h"

MenuState::MenuState() 
{
    prevItem = currentItem = 0;
    items = nullptr;
}

MenuState::~MenuState() 
{
    if (items != nullptr)
    for(MenuItem *item : *items)
    {
        delete item;
    }
    delete items;
}

void MenuState::render(GameEngine *engine) 
{
    if (items != nullptr)
        for(MenuItem *item : *items)
        {
            item->render();
        }
}

void MenuState::processInput(GameEngine *engine) 
{
    if (items == nullptr)
        backToMain(engine, this);
    SDL_Event event;
    SDL_PollEvent(&event);
    int x, y ;
    int buttons = SDL_GetMouseState(&x, &y);
    if (event.type == SDL_KEYDOWN)
    {
        if (event.key.keysym.sym == SDLK_RETURN)
        {
            items->at(currentItem)->activate(engine, this);
            return;
        }
        else if (event.key.keysym.sym == SDLK_ESCAPE && escapeAction != nullptr)
        {
            escapeAction(engine, this);
            return;
        }
        else if (event.key.keysym.sym == SDLK_DOWN)
        {
            items->at(currentItem)->setNotCurrent();
            currentItem = (currentItem + 1) % items->size();
            items->at(currentItem)->setCurrent();
            return;
        }
        else if (event.key.keysym.sym == SDLK_UP)
        {
            items->at(currentItem)->setNotCurrent();
            if (currentItem == 0)
                currentItem = items->size() - 1;
            else
                currentItem = (currentItem - 1);
            items->at(currentItem)->setCurrent();
            return;
        }
    }
    for(MenuItem *item : *items)
    {
        std::cerr << x << " " << y << endl;
        if(item->isMouseInside(x, y))
        {
            items->at(currentItem)->setNotCurrent();
            for (unsigned char i=0; i<items->size(); i++)
                if (items->at(i) == item)
                {
                    currentItem = i;
                    break;
                }
            item->setCurrent();
        }
    }
    if ((buttons & SDL_BUTTON(1)) && items->at(currentItem)->isMouseInside(x, y))
        items->at(currentItem)->activate(engine, this);
    standardEventsHandling(engine, event);
}

void MenuState::update(GameEngine *engine) 
{

}

void MenuState::exit(GameEngine *engine, MenuState* ms) 
{
    engine->shutdown();
}

void MenuState::start(GameEngine *engine, MenuState *ms) 
{
    ms->escapeAction = start;
    ms->items->at(ms->currentItem)->setText("Continue");
    engine->changeState(GameEngine::states::Play);
}

void MenuState::continuee(GameEngine *engine, MenuState *ms) 
{
    ms->escapeAction = continuee;
    engine->changeState(GameEngine::states::Play);
}

void MenuState::stats(GameEngine *engine, MenuState *ms)
{
    for(MenuItem *item : *(ms->items))
    {
        delete item;
    }
    delete ms->items;
    ms->items = new std::vector<MenuItem*>();
    ms->prevItem = ms->currentItem;
    ms->currentItem = 0;
    ms->items->push_back(new MenuItem(100, 100, "Back", backToMain));
    ms->items->at(0)->setCurrent();
    ms->escapeAction = backToMain;
}

void MenuState::backToMain(GameEngine *engine, MenuState *ms) 
{
    if (ms->items != nullptr)
    {
        for(MenuItem *item : *(ms->items))
        {
            delete item;
        }
        delete ms->items;
    }
    ms->items = nullptr;
    ms->items = new std::vector<MenuItem*>();
    ms->currentItem = ms->prevItem;
    ms->items->push_back(new MenuItem(70, engine->settings->getPixH() - 200, "Start", start));
    ms->items->push_back(new MenuItem(70, engine->settings->getPixH() - 150, "Stats", stats));
    ms->items->push_back(new MenuItem(70, engine->settings->getPixH() - 100, "Exit", exit));
    ms->items->at(ms->currentItem)->setCurrent();
    ms->escapeAction = nullptr;
}