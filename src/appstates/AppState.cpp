#include "AppState.h"
#include "../core/ResourcesManager.h"
#include "../core/GameEngine.h"

bool AppState::running = false;

AppState::AppState(){
    resMgr = new ResourcesManager();
}

AppState::~AppState(){
    delete resMgr;
}

void AppState::standardEventsHandling(GameEngine *engine, SDL_Event event){
    if(event.type == SDL_QUIT)
    {
        engine->shutdown();
    }
}
