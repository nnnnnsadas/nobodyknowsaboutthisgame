#include "PlayState.h"
#include "../core/GameEngine.h"
#include "../core/Settings.h"
#include "../components/Level.h"
#include "SDL_opengl.h"
#include "../components/Text.h"

PlayState::PlayState()
{
    camerax = cameray = 0;
    lvl = new Level(resMgr);
}

PlayState::~PlayState()
{
    delete lvl;
}

void PlayState::render(GameEngine *engine) 
{
    glTranslatef(int(camerax), int(cameray), 0);
    lvl->render(0, 0);
}

void PlayState::processInput(GameEngine *engine)
{
    SDL_Event event;
    SDL_PollEvent(&event);
    keystate = SDL_GetKeyboardState(0);
	if (event.type == SDL_KEYDOWN)
	{
		if (event.key.keysym.sym == SDLK_ESCAPE)
			engine->changeState(GameEngine::states::Menu);
	}
    standardEventsHandling(engine, event);
}

void PlayState::update(GameEngine *engine)
{
    if (keystate[SDL_SCANCODE_A])
        camerax += 0.5f;
    if (keystate[SDL_SCANCODE_D])
        camerax -= 0.5f;
    if (keystate[SDL_SCANCODE_W])
        cameray += 0.5f;
    if (keystate[SDL_SCANCODE_S])
        cameray -= 0.5f;
}
