#ifndef MENUSTATE_H_
#define MENUSTATE_H_

#include <SDL.h>
#include <SDL_opengl.h>
#include <GL/gl.h>
#include <vector>

#include "AppState.h"
#include "../core/ResourcesManager.h"

class MenuItem;

class MenuState: public AppState {
private:
    unsigned char currentItem, prevItem;
    std::vector<MenuItem*> *items;
    void (*escapeAction)(GameEngine*, MenuState*);
public:
    MenuState();
    virtual ~MenuState();
    void render(GameEngine *engine);
    void processInput(GameEngine *engine);
    void update(GameEngine *engine);
    // Actions for MenuItems
    static void exit(GameEngine*, MenuState*);
    static void start(GameEngine*, MenuState*);
    static void continuee(GameEngine*, MenuState*);
    static void stats(GameEngine*, MenuState*);
    static void settings(GameEngine*, MenuState*);
    static void backToMain(GameEngine*, MenuState*);
    static void toggleFullscreen(GameEngine*, MenuState*);
};

#endif /* MENUSTATE_H_ */
