#include "IntroState.h"
#include "../core/Settings.h"
#include "../core/GameEngine.h"

IntroState::IntroState(std::vector<std::string> introImages) 
{
    curTime = 0;
    for(std::string s : introImages)
    {
        queue.push(resMgr->get<Texture>(s));
    }
}

IntroState::~IntroState()
{

}

void IntroState::render(GameEngine *engine)
{
    if(queue.size() != 0)
    {
        if (curTime <= fadeTime)
            glColor4f(1.f,1.f,1.f,curTime / fadeTime);
        else if (maxTime - curTime <= fadeTime)
            glColor4f(1.f,1.f,1.f, (maxTime - curTime) / fadeTime);
        queue.front()->render(0, 0);
    }
}

void IntroState::processInput(GameEngine *engine)
{
    SDL_Event event;
    SDL_PollEvent(&event);
    if (event.type == SDL_KEYDOWN)
    {
        if (event.key.keysym.sym == SDLK_RETURN)
        {
            curTime = 0;
            queue.pop();
            if (queue.size() == 0)
            {
                engine->changeState(GameEngine::states::Menu);
                return;
            }
        }
    }
    if (curTime >= maxTime)
    {
        if (queue.size() <= 1)
        {
            engine->changeState(GameEngine::states::Menu);
            return;
        }
        else
        {
            queue.pop();
            curTime = 0;
        }
    }
    standardEventsHandling(engine, event);
}

void IntroState::update(GameEngine *engine)
{
    curTime += 25;
}
